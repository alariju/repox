class SolutionData:
    def __init__(self):
        self.lambdas = []
        self.mus = []
        self.AProbabilities = []
        self.SProbabilities = []
        self.amoutQueues = 0

    def printData(self):
        print("Lambdas: " + str(self.lambdas))
        print("Mus: " + str(self.mus))
        print("Probabilities: " + str(self.AProbabilities))

def open_file(pFileName):
	try:
		_file = open(pFileName)
		_lines = _file.readlines()
		_file.close()
		return _lines
	except:
		print("ERROR: Errow when opening the file. "+
			"The path or the name could be wrong")
		return False
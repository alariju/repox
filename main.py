import FileManagement, Simulation, sys

print("\nQueues & Markov's Chains\n\n")
_fileName = input("Enter the file name containing the parameters: ")
_duration = int(input("Enter the duration: "))

_fileLines = FileManagement.open_file(_fileName)
if(not _fileLines):
	sys.exit("")

Sim = Simulation.Simulation(_duration)
Sim.set_variables(_fileLines)
Sim.start_queues()